<?php

use yii\db\Migration;
use yii\db\Schema;

class m160520_152418_user_avatar_field extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'avatar', Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'avatar');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
