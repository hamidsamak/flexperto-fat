<?php

use yii\db\Migration;
use yii\db\Schema;

class m160520_152435_user_mobile_field extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'mobile', Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'mobile');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
