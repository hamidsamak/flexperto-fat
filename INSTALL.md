Installation
============

1. Run `./init` and select project mode.
2. Check database connection information in `common/config/main-local.php`
3. Run `composer update` to install/update dependencies.
4. Run `./yii migrate`
5. Browse project frontend `http://host/path/to/project/frontend/web`



As mentioned in README.md one of dependencies is `composer-asset-plugin:1.0.0`, but after updating composer packages the project shows error. For fixing this issue you need to use newer version of composer-asset-plugin using `composer global require "fxp/composer-asset-plugin:1.1.1"`.
It is highly recommended to delete installed version (`/home/USER/.config/composer/vendor/fxp`) before installing new version.