<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * show user profile model
 */
class ProfileView extends Model
{
    /**
     * check user exists or not
     * @param  string  $username
     * @return boolean
     */
    public function usernameExists($username) {
        // user object
        $user = User::findByUsername($username);
        
        if (empty($user))
            return false;

        return true;
    }
}
