<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * delete user account model
 */
class AccountDelete extends Model
{
	public $delete;

    /**
     * delete user account and avatar file
     * @return boolean
     */
	public function deleteAccount() {
        $user_identity = Yii::$app->user->identity;

        if (empty($user_identity->avatar) === false) {
        	$avatar_file = 'avatars/' . $user_identity->avatar;

        	if (file_exists($avatar_file))
        		unlink($avatar_file);
        }

        if (User::findIdentity($user_identity->id)->delete())
        	return true;

        return false;
    }
}
