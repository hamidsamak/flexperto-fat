<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * edit user profile model
 */
class ProfileEdit extends Model
{
    public $avatar;
    public $mobile;

    public function rules()
    {
        return [
            ['avatar', 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, gif'],
            ['mobile', 'filter', 'filter' => 'trim'],
            ['mobile', 'string', 'min' => 13]
        ];
    }

    /**
     * user update
     * @param  array   $params update parameters
     * @return boolean
     */
    public function profileUpdate($params) {
        $user = User::findIdentity(Yii::$app->user->identity->id);

        $user->avatar = $params['avatar_file'];

        if (empty($params['mobile']) || (is_numeric($params['mobile']) && $params['mobile'] > 0))
            $user->mobile = $params['mobile'];

        if ($user->update() !== false)
            return true;

        return false;
    }
}
