<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use common\models\User;
use frontend\models\ProfileView;
use frontend\models\ProfileEdit;
use yii\web\UploadedFile;
use yii\helpers\Url;
use frontend\models\AccountDelete;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * profile
     * @param  string $username
     * @return string
     */
    public function actionProfile($username = null) {
        
        if (empty($username)) { // profile edit page for current signed-in user

            // redirect to login page if not signed-in
            if (Yii::$app->user->isGuest === true)
                return $this->redirect(array('login'), 302);
            
            return $this->profileEdit();

        } else {

            $model = new ProfileView();
            
            // error page if user not exists
            if ($model->usernameExists($username) === false)
                return $this->render('error', ['name' => 'Profile not found', 'message' => 'No user specified for given address.']);

            return $this->profileView($username);
        }       
    }

    /**
     * profile edit
     * @return string
     */
    private function profileEdit() {
        $model = new ProfileEdit();

        // user identity object
        $user_identity = Yii::$app->user->identity;

        // form is submitted
        if (Yii::$app->request->isPost) {

            // get uploaded file
            $model->avatar = UploadedFile::getInstance($model, 'avatar');

            // validate form
            if ($model->validate()) {

                // is file submitted
                if ($model->avatar) {

                    // delete previous avatar if file found
                    if (empty($user_identity->avatar) === false) {
                        $current_avatar_path = 'avatars/' . $user_identity->avatar;

                        if (file_exists($current_avatar_path))
                            unlink($current_avatar_path);

                        unset($current_avatar_path);
                    }

                    // generate new avatar file name
                    $new_avatar = $user_identity->username . '_' . time() . '.' . $model->avatar->extension;

                    // save file to "avatars" directory
                    $model->avatar->saveAs('avatars/' . $new_avatar);

                } else
                    $new_avatar = $user_identity->avatar;

                // get mobile from posted data
                $new_mobile = Yii::$app->request->post()['ProfileEdit']['mobile'];

                // update profile fields
                $model->profileUpdate([
                    'avatar_file' => $new_avatar,
                    'mobile' => $new_mobile
                ]);
            }
        }

        // set avatar file if uploaded or get current avatar
        $avatar_file = isset($new_avatar) ? $new_avatar : $user_identity->avatar;

        // set default profile avatar if no avatar is uploaded
        if (empty($avatar_file))
            $avatar_file = 'none.png';

        // set mobile or use current value
        $model->mobile = isset($new_mobile) && (empty($new_mobile) || is_numeric($new_mobile)) ? $new_mobile : $user_identity->mobile;

        return $this->render('profile_edit', [
            'model' => $model,
            'profile_url' => Url::to(['site/profile', 'username' => $user_identity->username]),
            'avatar_url' => Yii::$app->request->BaseUrl . '/avatars/' . $avatar_file
        ]);
    }

    /**
     * profile view
     * @return string
     */
    private function profileView($username) {
        $user_identity = User::findByUsername($username);

        // values for browsing user profile
        $avatar_file = $user_identity->avatar;
        $mobile = $user_identity->mobile;

        // default avatar if is empty
        if (empty($avatar_file))
            $avatar_file = 'none.png';

        // single dash in mobile field if is empty
        // or show in format if length is equal to 13
        if (empty($mobile))
            $mobile = '-';
        else if (strlen($mobile) === 13)
            $mobile = '+' . preg_replace("/^\+(\d{2})(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3-$4", $mobile);

        return $this->render('profile_view', [
            'username' => $username,
            'avatar_url' => Yii::$app->request->BaseUrl . '/avatars/' . $avatar_file,
            'mobile' => $mobile
        ]);
    }

    /**
     * delete user account
     * @return string
     */
    public function actionDelete() {

        // redirect to home page if user is not signed-in
        if (Yii::$app->user->isGuest === true)
            return $this->goHome();

        $model = new AccountDelete();

        // delete account if form is confirmed
        if (Yii::$app->request->isPost && Yii::$app->request->post()['AccountDelete']['delete']) {
            $model->deleteAccount();

            return $this->goHome();
        }

        return $this->render('delete_account', ['model' => $model]); 
    }
}
