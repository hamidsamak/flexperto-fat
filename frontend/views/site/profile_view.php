<?php
use yii\helpers\Html;

$this->title = 'Profile';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-profile-edit">
	<h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-2">
        	<img src="<?= $avatar_url ?>" alt="Avatar" class="avatar">
        </div>
        <div class="col-lg-10">
        	<label>Username:</label> <?= Html::encode($username) ?><br>
        	<label>Mobile:</label> <?= Html::encode($mobile) ?>
        </div>
    </div>
</div>
