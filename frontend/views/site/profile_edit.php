<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Edit profile';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-profile-edit">
	<h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">

        	<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

            <div class="form-group field-profileedit-avatar-preview">
                <a href="<?= $profile_url ?>"><img src="<?= $avatar_url ?>" alt="Avatar" class="avatar"></a>
            </div>

			<?= $form->field($model, 'avatar')->fileInput() ?>
			<?= $form->field($model, 'mobile')->hint('Format: +112223334444') ?>
			<div class="form-group">
                    <?= Html::submitButton('Save changes', ['class' => 'btn btn-success', 'name' => 'save-changes']) ?>
                </div>
			<?php ActiveForm::end() ?>

            <div class="form-group">
                <hr>
                <label>Cancel account</label><br>
                <p>Click on delete button if you want to delete your account</p>
                <a href="<?= Yii::$app->homeUrl . '?r=' . Url::to('site/delete')?>" class="btn btn-danger btn-xs">Delete account</a>
            </div>
        </div>
    </div>
</div>
