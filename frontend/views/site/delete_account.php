<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Delete account';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-delete-account">
	<h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">

        	<?php $form = ActiveForm::begin() ?>

            <p>Are you sure to delete this account?<br>Your account and profile will be <strong>permanently deleted</strong>.</p>

			<?= $form->field($model, 'delete')->checkBox(['value' => '1', 'label' => 'Yes, I\'m sure.']) ?>
			
			<div class="form-group">
                    <?= Html::submitButton('Delete', ['class' => 'btn btn-danger', 'name' => 'delete-account']) ?>
                    <a href="<?= Yii::$app->homeUrl . '?r=' . Url::to('site/profile')?>" class="btn btn-default">Back</a>
                </div>
			<?php ActiveForm::end() ?>
        </div>
    </div>
</div>
