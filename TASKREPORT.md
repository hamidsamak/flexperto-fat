Task report
===========

1. Add an Avatar to Users
User would be able to upload avatar in profile page `index.php?r=site/profile`.

2. Add a mobile number to Users
Same as above item in the same page.

3. Build a profile for registered Users (including regular show, editing and cancelling the account)
A. Profile show `index.php?r=site/profile&username=$username`
B. Profile edit `index.php?r=site/profile` (login required)
C. Cancel account `index.php?r=size/profile` (login required)